﻿using ApplicationCore.Entities;

namespace ApplicationCore.Specifications
{
    public class ClientApiSpecification : SpecificationQuery<ClientApi>
    {
        public ClientApiSpecification(string clientId, string clientSecret)
            : base(c => c.ClientId == clientId && c.ClientSecret == clientSecret)
        {
            ApplyOrderBy(c => c.ClientId);
        }
    }
}

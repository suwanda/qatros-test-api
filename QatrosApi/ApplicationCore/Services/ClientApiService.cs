﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.Entities;
using ApplicationCore.Helpers.BaseEntity;
using ApplicationCore.Helpers.BaseEntity.Model;
using ApplicationCore.Interfaces;
using ApplicationCore.Interfaces.BaseEntity;

namespace ApplicationCore.Services
{
    public class ClientApiService : IClientApiService
    {
        private readonly IEfRepository<ClientApi, Guid> _repository;

        public ClientApiService(IEfRepository<ClientApi, Guid> repository)
        {
            _repository = repository;
        }

        public async Task<IReadOnlyList<ClientApi>> GetAllAsync()
        {
            return await _repository.GetAllAsync();
        }

        public async Task<ClientApi> GetAsync(Guid id)
        {
            return await _repository.GetAsync(id);
        }

        public async Task<Guid?> PostAsync(ClientApi model)
        {
            Guid? id = null;

            var whereData = new Dictionary<string, object>
            {
                { nameof(model.ClientId), model.ClientId}
            };

            if (!await _repository.GetAll().IsExistValueAsync(whereData))
            {
                var data = await _repository.AddAsync(model);
                id = data.Id;
            }

            return id;
        }

        public async Task<Guid?> PutAsync(ClientApi model)
        {
            Guid? id = null;

            var whereData = new Dictionary<string, object>
            {
                { nameof(model.ClientId), model.ClientId}
            };

            if (!await _repository.GetAll().IsExistValueWithKeyAsync(nameof(model.Id), model.Id, nameof(model.ClientId), model.ClientId, whereData))
            {
                await _repository.UpdateAsync(model);
                id = model.Id;
            }

            return id;
        }

        public async Task DeleteAsync(Guid id)
        {
            var data = await _repository.GetAsync(id);
            await _repository.DeleteAsync(data);
        }

        public async Task<bool> IsExistValueAsync(IDictionary<string, object> whereData)
        {
            return await _repository.IsExistValueAsync(whereData);
        }

        public async Task<bool> IsExistValueWithKeyAsync(ExistWithKeyModel model)
        {
            return await _repository.IsExistValueWithKeyAsync(model);
        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApplicationCore.Helpers.BaseEntity.Model;
using ApplicationCore.Interfaces.Query;

namespace ApplicationCore.Interfaces.BaseEntity
{
    public interface IEfRepository<TEntity, TId> where TEntity : class, IModel<TId>
    {
        IQueryable<TEntity> GetAll();

        IQueryable<TEntity> GetAll(ISpecificationQuery<TEntity> spec);

        Task<IReadOnlyList<TEntity>> GetAllAsync();

        Task<IReadOnlyList<TEntity>> GetAllAsync(ISpecificationQuery<TEntity> spec);

        Task<TEntity> GetAsync(ISpecificationQuery<TEntity> spec);

        Task<TEntity> GetAsync(TId id);

        Task<int> CountAllAsync();

        Task<int> CountAsync(ISpecificationQuery<TEntity> spec);

        Task<TEntity> AddAsync(TEntity entity);

        Task UpdateAsync(TEntity entity);

        Task DeleteAsync(TEntity entity);

        Task<bool> IsExistValueAsync(IDictionary<string, object> whereData);

        Task<bool> IsExistValueWithKeyAsync(ExistWithKeyModel model);
    }
}

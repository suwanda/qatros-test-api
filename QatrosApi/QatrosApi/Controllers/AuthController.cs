﻿using System.Threading.Tasks;
using ApplicationCore.Helpers;
using ApplicationCore.Interfaces.Auth;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly IAuthService _authService;

        public AuthController(IConfiguration config, IAuthService authService)
        {
            _config = config;
            _authService = authService;
        }

        // GET: api/Auth/token
        [HttpGet("token/{clientId}/{clientSecret}")]
        public async Task<ActionResult> Token(string clientId, string clientSecret)
        {
            ActionResult response = Unauthorized();

            if (clientSecret.IsBase64())
            {
                var tokens = await _authService.GenerateJwtAsync(clientId, clientSecret.ToBase64DecodeWithKey(_config["Security:EncryptKey"]));

                if (tokens.Count > 0)
                {
                    response = Ok(new { token = tokens[0], validFrom = tokens[1], validTo = tokens[2] });
                }
            }

            return response;
        }

        // GET: api/Auth/user/tenant
        [HttpGet("user/login/{username}/{password}")]
        public ActionResult UserLogin(string username, string password)
        {
            if (username == "tbudinam" && password == "123")
            {

            }

            if (username == "mk.suwanda1" && password == "123")
            {

            }

            return Ok("9f453e76-a6bc-4708-a013-a7281073b01a");
        }
    }
}

﻿using System.Collections.Generic;
using ApplicationCore.Entities;

namespace ApplicationCore.Interfaces.Auth
{
    public interface IJwt
    {
        List<string> GetJwt(ClientApi clientApi);
    }
}

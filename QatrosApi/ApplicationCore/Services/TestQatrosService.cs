﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.Entities;
using ApplicationCore.Helpers.BaseEntity;
using ApplicationCore.Helpers.BaseEntity.Model;
using ApplicationCore.Interfaces;
using ApplicationCore.Interfaces.BaseEntity;
using Microsoft.EntityFrameworkCore;

namespace ApplicationCore.Services
{
    public class TestQatrosService : ITestQatrosService
    {
        private readonly IEfRepository<TestQatros, Guid> _repository;

        public TestQatrosService(IEfRepository<TestQatros, Guid> repository)
        {
            _repository = repository;
        }

        public async Task<IReadOnlyList<TestQatros>> GetAllAsync()
        {
            return await _repository.GetAllAsync();
        }

        public async Task<TestQatros> GetAsync(Guid id)
        {
            return await _repository.GetAll().FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<Guid?> PostAsync(TestQatros model)
        {
            Guid? id = null;

            var whereData = new Dictionary<string, object>
            {
                { nameof(model.Name), model.Name}
            };

            if (!await _repository.GetAll().IsExistValueAsync(whereData))
            {
                var data = await _repository.AddAsync(model);
                id = data.Id;
            }

            return id;
        }

        public async Task<Guid?> PutAsync(TestQatros model)
        {
            Guid? id = null;

            var whereData = new Dictionary<string, object>
            {
                { nameof(model.Name), model.Name}
            };

            if (!await _repository.GetAll().IsExistValueWithKeyAsync(nameof(model.Id), model.Id, nameof(model.Name), model.Name, whereData))
            {
                await _repository.UpdateAsync(model);
                id = model.Id;
            }

            return id;
        }

        public async Task DeleteAsync(Guid id)
        {
            var data = await _repository.GetAsync(id);
            await _repository.DeleteAsync(data);
        }

        public async Task<bool> IsExistValueAsync(IDictionary<string, object> whereData)
        {
            return await _repository.IsExistValueAsync(whereData);
        }

        public async Task<bool> IsExistValueWithKeyAsync(ExistWithKeyModel model)
        {
            return await _repository.IsExistValueWithKeyAsync(model);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using ApplicationCore.Helpers.BaseEntity.Model;
using ApplicationCore.Interfaces.BaseEntity;
using ApplicationCore.Interfaces.Query;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Data
{
    public class EfRepository<TEntity, TId> : IEfRepository<TEntity, TId> where TEntity : class, IModel<TId>
    {
        protected readonly ApplicationDbContext _dbContext;

        public EfRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IQueryable<TEntity> GetAll()
        {
            var data = _dbContext.Set<TEntity>().AsNoTracking();

            foreach (var item in EntityIncludes())
            {
                data = data.Include(item);
            }

            return data;
        }

        public IQueryable<TEntity> GetAll(ISpecificationQuery<TEntity> spec)
        {
            return ApplyQuery(spec);
        }

        public async Task<IReadOnlyList<TEntity>> GetAllAsync()
        {
            var data = _dbContext.Set<TEntity>().AsQueryable();
            foreach (var item in EntityIncludes())
            {
                data = data.Include(item);
            }

            return await data.AsNoTracking().ToListAsync();
        }

        public virtual async Task<IReadOnlyList<TEntity>> GetAllAsync(ISpecificationQuery<TEntity> spec)
        {
            return await ApplyQuery(spec).AsNoTracking().ToListAsync();
        }

        public virtual async Task<TEntity> GetAsync(ISpecificationQuery<TEntity> spec)
        {
            return await ApplyQuery(spec).FirstOrDefaultAsync();
        }

        public virtual async Task<TEntity> GetAsync(TId id)
        {
            var query = _dbContext.Set<TEntity>().AsQueryable();
            foreach (var item in EntityIncludes())
            {
                query = query.Include(item);
            }

            return await query.SingleOrDefaultAsync(x => x.Id.Equals(id));
        }

        public async Task<int> CountAllAsync()
        {
            return await _dbContext.Set<TEntity>().CountAsync();
        }

        public async Task<int> CountAsync(ISpecificationQuery<TEntity> spec)
        {
            return await ApplyQuery(spec).CountAsync();
        }

        public async Task<TEntity> AddAsync(TEntity entity)
        {
            await _dbContext.Set<TEntity>().AddAsync(entity);
            await _dbContext.SaveChangesAsync();

            return entity;
        }

        public async Task UpdateAsync(TEntity entity)
        {
            _dbContext.Entry(entity).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();
        }

        public async Task DeleteAsync(TEntity entity)
        {
            _dbContext.Set<TEntity>().Remove(entity);
            await _dbContext.SaveChangesAsync();
        }

        public virtual async Task<bool> IsExistValueAsync(IDictionary<string, object> whereData)
        {
            var dbSet = _dbContext.Set<TEntity>().AsNoTracking();
            IQueryable<TEntity> exp;

            var whereCriteria = string.Empty;

            int i = 1;
            foreach (var item in whereData)
            {
                if (item.Value.GetType() == typeof(string) || item.Value.GetType() == typeof(Guid) || item.Value.GetType() == typeof(DateTime))
                {
                    whereCriteria += item.Key + "=\"" + item.Value + "\"";
                }
                else if (item.Value.GetType() == typeof(DateTime))
                {
                    var dateValue = DateTime.Parse(item.Value.ToString());
                    var dateValueAdd = DateTime.Parse(item.Value.ToString()).AddDays(1);

                    whereCriteria += item.Key + " >= DateTime(" + dateValue.Year + ", " + dateValue.Month + ", " + dateValue.Day + ") and " + item.Key + " < DateTime(" + dateValueAdd.Year + ", " + dateValueAdd.Month + ", " + dateValueAdd.Day + ")";
                }
                else
                {
                    whereCriteria += item.Key + "=" + item.Value;
                }

                if (i < whereData.Count)
                {
                    whereCriteria += " and ";
                }

                i++;
            }

            exp = dbSet.Where(whereCriteria);
            var data = await exp.AnyAsync();
            return data;
        }

        public virtual async Task<bool> IsExistValueWithKeyAsync(ExistWithKeyModel model)
        {
            var dbSet = _dbContext.Set<TEntity>().AsNoTracking();
            IQueryable<TEntity> oldData;

            if (model.KeyValue.GetType() == typeof(string) || model.KeyValue.GetType() == typeof(Guid))
            {
                oldData = dbSet.Where(model.KeyName + "=\"" + model.KeyValue + "\"");
            }
            else
            {
                oldData = dbSet.Where(model.KeyName + "=" + model.KeyValue);
            }

            var resulOldtData = oldData.Select(model.FieldName).ToDynamicList();
            var oldValue = resulOldtData[0];

            bool result;

            if (oldValue != model.FieldValue)
            {
                IQueryable<TEntity> exp;

                var whereCriteria = string.Empty;

                int i = 1;
                foreach (var item in model.WhereData)
                {
                    if (item.Value.GetType() == typeof(string) || item.Value.GetType() == typeof(Guid))
                    {
                        whereCriteria += item.Key + "=\"" + item.Value + "\"";
                    }
                    else if (item.Value.GetType() == typeof(DateTime))
                    {
                        var dateValue = DateTime.Parse(item.Value.ToString());
                        var dateValueAdd = DateTime.Parse(item.Value.ToString()).AddDays(1);

                        whereCriteria += item.Key + " >= DateTime(" + dateValue.Year + ", " + dateValue.Month + ", " + dateValue.Day + ") and " + item.Key + " < DateTime(" + dateValueAdd.Year + ", " + dateValueAdd.Month + ", " + dateValueAdd.Day + ")";
                    }
                    else
                    {
                        whereCriteria += item.Key + "=" + item.Value;
                    }

                    if (i < model.WhereData.Count)
                    {
                        whereCriteria += " and ";
                    }

                    i++;
                }

                exp = dbSet.Where(whereCriteria);
                result = await exp.AnyAsync();
            }
            else
            {
                if (model.WhereData.Count > 1)
                {
                    IQueryable<TEntity> exp;

                    var whereCriteria = string.Empty;

                    int i = 1;
                    foreach (var item in model.WhereData)
                    {
                        if (item.Value.GetType() == typeof(string) || item.Value.GetType() == typeof(Guid))
                        {
                            whereCriteria += item.Key + "=\"" + item.Value + "\"";
                        }
                        else if (item.Value.GetType() == typeof(DateTime))
                        {
                            var dateValue = DateTime.Parse(item.Value.ToString());
                            var dateValueAdd = DateTime.Parse(item.Value.ToString()).AddDays(1);

                            whereCriteria += item.Key + " >= DateTime(" + dateValue.Year + ", " + dateValue.Month + ", " + dateValue.Day + ") and " + item.Key + " < DateTime(" + dateValueAdd.Year + ", " + dateValueAdd.Month + ", " + dateValueAdd.Day + ")";
                        }
                        else
                        {
                            whereCriteria += item.Key + "=" + item.Value;
                        }

                        if (i < model.WhereData.Count)
                        {
                            whereCriteria += " and ";
                        }

                        i++;
                    }

                    exp = dbSet.Where(whereCriteria);
                    result = await exp.AnyAsync();
                }
                else
                {
                    result = false;
                }
            }

            return result;
        }

        private IQueryable<TEntity> ApplyQuery(ISpecificationQuery<TEntity> spec)
        {
            var data = _dbContext.Set<TEntity>().AsQueryable();
            foreach (var item in EntityIncludes())
            {
                data = data.Include(item);
            }

            return BaseSpecificationQuery<TEntity>.GetQuery(data, spec);
        }

        protected List<string> EntityIncludes()
        {
            var includes = new List<string>();
            foreach (var property in _dbContext.Model.FindEntityType(typeof(TEntity)).GetNavigations())
            {
                includes.Add(property.Name);
            }

            return includes;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.Entities;
using ApplicationCore.Helpers.BaseEntity.Model;

namespace ApplicationCore.Interfaces
{
    public interface IClientApiService
    {
        Task<IReadOnlyList<ClientApi>> GetAllAsync();

        Task<ClientApi> GetAsync(Guid id);

        Task<Guid?> PostAsync(ClientApi model);

        Task<Guid?> PutAsync(ClientApi model);

        Task DeleteAsync(Guid id);

        Task<bool> IsExistValueAsync(IDictionary<string, object> whereData);

        Task<bool> IsExistValueWithKeyAsync(ExistWithKeyModel model);
    }
}

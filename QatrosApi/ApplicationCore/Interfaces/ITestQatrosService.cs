﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.Entities;
using ApplicationCore.Helpers.BaseEntity.Model;

namespace ApplicationCore.Interfaces
{
    public interface ITestQatrosService
    {
        Task<IReadOnlyList<TestQatros>> GetAllAsync();

        Task<TestQatros> GetAsync(Guid id);

        Task<Guid?> PostAsync(TestQatros model);

        Task<Guid?> PutAsync(TestQatros model);

        Task DeleteAsync(Guid id);

        Task<bool> IsExistValueAsync(IDictionary<string, object> whereData);

        Task<bool> IsExistValueWithKeyAsync(ExistWithKeyModel model);
    }
}

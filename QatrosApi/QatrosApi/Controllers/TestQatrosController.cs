﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.Entities;
using ApplicationCore.Helpers.Api.Response;
using ApplicationCore.Helpers.ApiResponse;
using ApplicationCore.Helpers.BaseEntity;
using ApplicationCore.Helpers.BaseEntity.Model;
using ApplicationCore.Interfaces;
using ApplicationCore.Interfaces.Logging;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    public class TestQatrosController : BaseApiController
    {
        private readonly ITestQatrosService _service;
        private readonly IAppLogger<TestQatros> _loger;

        public TestQatrosController(ITestQatrosService service, IAppLogger<TestQatros> loger)
        {
            _service = service;
            _loger = loger;
        }

        [HttpGet("get/all")]
        public async Task<IActionResult> GetAllAsync()
        {
            var data = await _service.GetAllAsync();
            var result = new OK("Success get all data", data);
            return result.ReturnResponse();
        }

        [HttpGet("get/{id}")]
        public async Task<IActionResult> GetAsync(Guid id)
        {
            var data = await _service.GetAsync(id);
            if (data == null)
            {
                _loger.LogError("Data not found for id : " + id.ToString());

                var error = new NotFound("Data not found for id : " + id.ToString(), new { id });
                return error.ReturnResponse();
            }

            var result = new OK("Success get data by id : " + id.ToString(), data);
            return result.ReturnResponse();
        }

        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody] TestQatros data)
        {
            if (data == null)
            {
                _loger.LogError("Bad request parameter");

                var error = new BadRequest("Parameter is null", new { data });
                return error.ReturnResponse();
            }

            var id = await _service.PostAsync(data);
            if (id == null)
            {
                _loger.LogError("Duplicate data for name : " + data.Name);

                var error = new Forbidden("Duplicate data for name : " + data.Name, new { data });
                return error.ReturnResponse();
            }

            var result = new OK("Success add data", new { id });
            return result.ReturnResponse();
        }

        [HttpPut]
        public async Task<IActionResult> PutAsync([FromBody] TestQatros data)
        {
            if (data == null)
            {
                _loger.LogError("Bad request parameter");

                var error = new BadRequest("Parameter is null", new { data });
                return error.ReturnResponse();
            }

            var id = await _service.PutAsync(data);
            if (id == null)
            {
                _loger.LogError("Duplicate data for name : " + data.Name);

                var error = new Forbidden("Duplicate data for name : " + data.Name, new { data });
                return error.ReturnResponse();
            }

            var result = new OK("Success update data", new { id });
            return result.ReturnResponse();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(Guid id)
        {
            var data = await _service.GetAsync(id);
            if (data == null)
            {
                _loger.LogError("Not found data for id : " + id.ToString());

                var error = new NotFound("Not found data for id : " + id.ToString(), new { id });
                return error.ReturnResponse();
            }

            await _service.DeleteAsync(id);
            var result = new OK("Success delete data " + data.Name, new { id });
            return result.ReturnResponse();
        }

        [HttpPost("exist")]
        public async Task<IActionResult> IsExistValueAsync([FromBody] IDictionary<string, object> whereData)
        {
            if (whereData == null)
            {
                _loger.LogError("Bad request parameter");

                var error = new BadRequest("Bad request parameter", new { whereData });
                return error.ReturnResponse();
            }

            var exist = await _service.IsExistValueAsync(whereData);

            var result = new OK("Success check exist data", new { exist });
            return result.ReturnResponse();
        }

        [HttpPost("exist/key")]
        public async Task<IActionResult> IsExistValueWithKeyAsync([FromBody] ExistWithKeyModel model)
        {
            if (string.IsNullOrEmpty(model.FieldName)
                || string.IsNullOrEmpty(model.FieldName))
            {
                _loger.LogError("Bad Request Parameter");

                var error = new BadRequest("Bad Request Parameter", new { model });
                return error.ReturnResponse();
            }
            else if (typeof(TestQatros).HasProperty(model.KeyName.ToLower()) && typeof(TestQatros).HasProperty(model.FieldName.ToLower()))
            {
                _loger.LogError("Property name does not exist");

                var error = new BadRequest("Property name does not exist", new { model });
                return error.ReturnResponse();
            }

            var exist = await _service.IsExistValueWithKeyAsync(model);

            var result = new OK("Success check exist data with key " + model.FieldName + " : " + model.FieldValue, new { exist });
            return result.ReturnResponse();
        }
    }
}
